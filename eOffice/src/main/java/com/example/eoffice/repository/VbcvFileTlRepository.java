package com.example.eoffice.repository;


import com.example.eoffice.model.VbcvFileDt;
import com.example.eoffice.model.VbcvFileTl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VbcvFileTlRepository extends JpaRepository <VbcvFileTl,Integer> {
    @Query("SELECT b FROM VbcvFileTl b WHERE NOT EXISTS " +
            "(select 1 from VbcvFileTl old WHERE b.fileTlId = old.fileTlId)")
    List<VbcvFileTl> getListVbcvFileTL();
}

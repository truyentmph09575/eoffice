package com.example.eoffice.repository;

import com.example.eoffice.model.VbcvFileDt;
import com.example.eoffice.model.VbcvFileVdi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VbcvFileVdiRepository extends JpaRepository<VbcvFileVdi,Integer > {
    @Query("SELECT b FROM VbcvFileVdi b WHERE NOT EXISTS " +
            "(select 1 from VbcvFileVdi old WHERE b.FileVbdiId = old.FileVbdiId)")
    List<VbcvFileVdi> getListVbcvFileVbdi();
}

package com.example.eoffice.repository;


import com.example.eoffice.model.VbcvFileDt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VbcvFileDtRepository extends JpaRepository  <VbcvFileDt,Integer >  {
    @Query("SELECT b FROM VbcvFileDt b WHERE NOT EXISTS " +
            "(select 1 from VbcvFileDt old WHERE b.fileDtId = old.fileDtId)")
    List<VbcvFileDt> getListVbcvFileDt();

}

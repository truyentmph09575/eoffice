package com.example.eoffice.repository;


import com.example.eoffice.model.VbcvVanBanDi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VbcvVanBanDiRepository extends JpaRepository<VbcvVanBanDi, Integer> {
    @Query("SELECT b FROM VbcvVanBanDi b WHERE NOT EXISTS " +
            "(select 1 from VbcvVanBanDi old WHERE b.vbdiId = old.vbdiId)")
    List<VbcvVanBanDi> getListVbcvVanBanDi();
}

package com.example.eoffice.repository;

import com.example.eoffice.model.EOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EOfficeRepository extends JpaRepository<EOffice,Integer> {
    @Query("SELECT b FROM EOffice b WHERE NOT EXISTS " +
            "(select 1 from EOffice old WHERE b.id = old.id)")
    List<EOffice> getListEOffice();
}

package com.example.eoffice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "vbcv_file_dt")
public class VbcvFileDt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_dt_id")
    private int fileDtId;

    @Column(name = "ma_don_vi")
    private int maDonVi;

    @Column(name = "ten_file")
    private String tenFile;

    @Column(name = "noi_dung")
    private String noiDung;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "vbdt_id")
    private int vbdtId;

    @Column(name = "root_id")
    private int rootID;

    @Column(name = "databale_name")
    private String databaseName;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "ky_ca")
    private Boolean kyCA;

    @Column(name = "deleted")
    private Boolean deleted;

    @Column(name = "file_chinh")
    private Boolean fileChinh;

    @Column(name = "ten_file_vat_ly")
    private String tenFileVatLy;

    @Column(name = "ngay_tao")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayTao;
}

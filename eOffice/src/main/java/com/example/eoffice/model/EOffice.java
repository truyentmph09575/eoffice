package com.example.eoffice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "e_office")
public class EOffice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "trich_yeu")
    private String trichYeu;

    @Column(name = "so_ky_hieu")
    private int soKyHieu;

    @Column(name = "ngay_vb")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayVb;

    @Column(name = "ngay_tao")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayTao;

    @Column(name = "ten_file")
    private String tenFile;

    @Column(name = "status")
    private int status;
}

package com.example.eoffice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "vbcv_van_ban_di")
public class VbcvVanBanDi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vbdi_id")
    private int vbdiId;

    @Column(name = "ma_don_vi")
    private int maDonVi;

    @Column(name = "so_ky_hieu")
    private String soKyHieu;

    @Column(name = "so_di")
    private int soDi;

    @Column(name = "so_di_ky_tu")
    private String soDiKyTu;

    @Column(name = "trich_yeu")
    private String trichYeu;

    @Column(name = "ngay_vb")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayVb;

    @Column(name = "han_gq")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime hanGq;

    @Column(name = "tt_xl")
    private int ttXl;

    @Column(name = "noi_luu")
    private int noiLuu;

    @Column(name = "vi_tri_luu")
    private String viTriLuu;

    @Column(name = "nhan_bngoai")
    private String nhanBngoai;

    @Column(name = "nguoi_ky")
    private String nguoiKy;

    @Column(name = "sluong_vb")
    private int sluongVb;

    @Column(name = "so_trang")
    private int soTrang;

    @Column(name = "lich_su_cap_nhap")
    private String lichSuCapNhap;

    @Column(name = "noi_bo")
    private Boolean noiBo;

    @Column(name = "loai_vb_id")
    private int loaiVbId;

    @Column(name = "so_vb_id")
    private int soVbId;

    @Column(name = "ma_dk")
    private String maDk;

    @Column(name = "ma_dm")
    private String maDm;

    @Column(name = "ndung_xl")
    private String ndungXl;

    @Column(name = "ndung_xl_noi_bo")
    private String ndungXlNoiBo;

    @Column(name = "ngay_gui")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayGui;

    @Column(name = "dsvbde_id")
    private String dsvbdeId;

    @Column(name = "xac_nhan_chuyen")
    private Boolean xacNhanChuyen;

    @Column(name = "noi_soan_thao")
    private String noiSoanThao;


    @Column(name = "tra_loi_cho_cac_vb")
    private String traLoiChoCacVb;

    @Column(name = "nguoi_tao")
    private int nguoiTao;

    @Column(name = "vbdt_id")
    private int vbdtId;

    @Column(name = "ghi_chu")
    private String ghiChu;

    @Column(name = "nhan_noibo")
    private String nhanNoibo;

    @Column(name = "files")
    private Boolean files;

    @Column(name = "yc_vb_traloi")
    private Boolean ycVbTraloi;

    @Column(name = "han_traloi_vb")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime hanTraloiVb;

    @Column(name = "noteditdoc")
    private int noteditdoc;

    @Column(name = "van_thu")
    private Boolean vanThu;

    @Column(name = "nhom_gvb")
    private Boolean nhomGvb;

    @Column(name = "chuyen_nb")
    private Boolean chuyenNb;

    @Column(name = "chuyen_bn")
    private Boolean chuyenBn;

    @Column(name = "vbde_id_dvc_tren")
    private int vbdeIdDvcTren;

    @Column(name = "noinhan_dv_ctren")
    private String noinhanDvCtren;

    @Column(name = "noi_gui")
    private String noiGui;

    @Column(name = "chi_dao")
    private String chiDao;

    @Column(name = "tt_thu_hoi")
    private int ttThuHoi;

    @Column(name = "tt_bosungphuluc")
    private int ttBosungphuluc;

    @Column(name = "xem_tt_phanhoi")
    private Boolean xemTtPhanhoi;

    @Column(name = "so_lan_ky")
    private int soLanKy;

    @Column(name = "tra_lai")
    private Boolean traLai;

    @Column(name = "noi_nhan")
    private String noiNhan;

    @Column(name = "so_di_day_du")
    private String soDiDayDu;

    @Column(name = "noiluu")
    private String noiluu;

    @Column(name = "pb_nhanvb")
    private String pbNhanvb;

    @Column(name = "nhan_bngoai_dongbo")
    private String nhanBngoaiDongbo;

    @Column(name = "field_sign_all")
    private String fieldSignAll;

    @Column(name = "ngay_tao")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime ngayTao;

    @Column(name = "thong_bao")
    private int thongBao;

    @Column(name = "van_thu_ky")
    private int vanThuKy;


    @Column(name = "cp_nhanh")
    private Boolean cpNhanh;

    @Column(name = "xac_nhan_gui_don_vi")
    private Boolean xacNhanGuiDonVi;

    @Column(name = "noi_dung_tra_lai")
    private Boolean noiDungTraLai;

    @Column(name = "danh_dau_theo_doi")
    private int danhDauTheoDoi;

    @Column(name = "home_visible")
    private Boolean homeVisible;


    }



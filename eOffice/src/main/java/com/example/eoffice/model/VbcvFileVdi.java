package com.example.eoffice.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "vbcv_file_vbdi")
public class VbcvFileVdi {
    @Id
    @Column(name = "Ma_DonVi")
    private int MaDonVi;

    @Column(name = "File_VBDI_ID")
    private int FileVbdiId;

    @Column(name = "VBDI_ID")
    private int VbdiId;

    @Column(name = "Noi_Dung")
    private String NoiDung;

    @Column(name = "Ten_File")
    private String TenFile;

    @Column(name = "VBDT_ID")
    private int VBDTID;

    @Column(name = "Root_ID")
    private int RootID;


    @Column(name = "File_Chinh")
    private Boolean FileChinh;

    @Column(name = "VAITROKY")
    private String VaiTroKy;

    @Column(name = "Ngay_Tao")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime NgayTao;
}

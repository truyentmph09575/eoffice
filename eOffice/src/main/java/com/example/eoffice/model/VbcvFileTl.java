package com.example.eoffice.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "vbcv_file_tl")
public class VbcvFileTl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "file_tl_id")
    private int fileTlId;

    @Column(name = "tl_id")
    private int tlId;


    @Column(name = "noi_dung")
    private Boolean noiDung;


    @Column(name = "ten_file")
    private String tenFile;

    @Column(name = "file_type")
    private String fileType;


}

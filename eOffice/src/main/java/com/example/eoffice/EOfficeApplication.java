package com.example.eoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EOfficeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EOfficeApplication.class, args);
    }

}

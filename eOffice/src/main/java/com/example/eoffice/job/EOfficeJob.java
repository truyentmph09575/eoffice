package com.example.eoffice.job;


import com.example.eoffice.model.*;
import com.example.eoffice.service.EOfficeJobService;
import lombok.extern.log4j.Log4j2;
import okhttp3.*;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Component
@Log4j2
public class EOfficeJob {
    long startTime = Instant.now().toEpochMilli();
    @Autowired
    private EOfficeJobService eOfficeService;

    public static void main(String[] args) {
        ArrayList<VbcvFileDt> listVbcvFileDt = new ArrayList<>();
        VbcvFileDt test = new VbcvFileDt();
        listVbcvFileDt.add(0, test);
        JSONArray arr = new JSONArray(listVbcvFileDt);
        String strRequest = "" +
                "{\r\n    " +
                "\"sessionId\": \"123456\",\r\n    " +
                "\"token\": \"123456\",\r\n    \"wsCode\": \"addVbcvFileDt\",\r\n    " +
                "\"wsRequest\": " +
                "{\r\n        " +
                "\"list\": " + arr +
                "}\r\n" +
                "}";
        log.info("Request : " + strRequest);

    }
    @Scheduled(cron = "12 * * * * *")
    public void sendApiEOffice() throws Exception {
        List<EOffice> listtest = eOfficeService.getListEOffice();
        if (ObjectUtils.isEmpty(listtest)) {
            JSONArray arr = new JSONArray(listtest);
            getApi(arr, "addEOffice");
        }
    }

//    @Scheduled(cron = "job.eEfficeJob")
    public void sendApiVbcvFileDt() throws Exception {
        List<VbcvFileDt> listBsxKhOrgSxNamForJob = eOfficeService.getListVbcvFileDt();
        if (ObjectUtils.isEmpty(listBsxKhOrgSxNamForJob)) {
            JSONArray arr = new JSONArray(listBsxKhOrgSxNamForJob);
            getApi(arr, "addVbcvFileDt");
        }
    }

//    @Scheduled(cron = "")
    public void sendApiVbcvFileTl() throws Exception {
        List<VbcvFileTl> listVbcvFileTl = eOfficeService.getListVbcvFileTl();
        if (ObjectUtils.isEmpty(listVbcvFileTl)) {
            JSONArray arr = new JSONArray(listVbcvFileTl);
            getApi(arr, "addVbcvFileTl");
        }
    }
//    @Scheduled(cron = "")
    public void sendApiVbcvFileVdi() throws Exception {
        List<VbcvFileVdi> listVbcvFileVdi = eOfficeService.getListVbcvFileVdi();
        if (ObjectUtils.isEmpty(listVbcvFileVdi)) {
            JSONArray arr = new JSONArray(listVbcvFileVdi);
            getApi(arr, "addVbcvFileVdi");
        }
    }
//    @Scheduled(cron = "")
    public void sendApiVbcvVanBanDi() throws Exception {
        List<VbcvVanBanDi> listVbcvVanBanDi = eOfficeService.getListVbcvVanBanDi();
        if (ObjectUtils.isEmpty(listVbcvVanBanDi)) {
            JSONArray arr = new JSONArray(listVbcvVanBanDi);
            getApi(arr, "insertVbcvVanBanDi");
        }
    }


    private void getApi(JSONArray arr, String nameApi) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        String strRequest = "" +
                "{\r\n    " +
                "\"sessionId\": \"123456\",\r\n    " +
                "\"token\": \"123456\",\r\n    \"wsCode\": \"" + nameApi + "\",\r\n    " +
                "\"wsRequest\": " +
                "{\r\n        " +
                "\"listEOffice\": " + arr +
                "}\r\n" +
                "}";
        log.info("Request : " + strRequest);
        RequestBody body = RequestBody.create(mediaType, strRequest);
        Request request = new Request.Builder()
                .url("http://localhost:8080/api/" + nameApi)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful()) {
            throw new IOException("Unexpected code " + response);
        }
        log.info("Time Taken (milliseconds): " + (Instant.now().toEpochMilli() - startTime));
        log.info("Date: " + response.header("Date"));
        log.info("Log API: " + response.body().string());
    }


}

package com.example.eoffice.service.impl;

import com.example.eoffice.model.*;
import com.example.eoffice.repository.*;
import com.example.eoffice.service.EOfficeJobService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@Log4j2
public class EOfficeJobServicelmpl implements EOfficeJobService {
    @Autowired
    VbcvFileDtRepository vbcvFileDtRepository;
    @Autowired
    VbcvFileTlRepository vbcvFileTlRepository;
    @Autowired
    VbcvFileVdiRepository vbcvFileVdiRepository;
    @Autowired
    VbcvVanBanDiRepository vbcvVanBanDiRepository;
    @Autowired
    EOfficeRepository eOfficeRepository;
    @Override
    public List<VbcvFileDt> getListVbcvFileDt() {
        return vbcvFileDtRepository.getListVbcvFileDt();
    }

    @Override
    public List<VbcvFileTl> getListVbcvFileTl() {
        return vbcvFileTlRepository.getListVbcvFileTL();
    }

    @Override
    public List<VbcvFileVdi> getListVbcvFileVdi() {
        return vbcvFileVdiRepository.getListVbcvFileVbdi();
    }

    @Override
    public List<VbcvVanBanDi> getListVbcvVanBanDi() {
        return vbcvVanBanDiRepository.getListVbcvVanBanDi();
    }

    @Override
    public List<EOffice> getListEOffice() {
        return eOfficeRepository.getListEOffice() ;
    }
}

package com.example.eoffice.service;

import com.example.eoffice.model.*;

import java.util.List;

public interface EOfficeJobService {
    List<VbcvFileDt> getListVbcvFileDt();

    List<VbcvFileTl> getListVbcvFileTl();

    List<VbcvFileVdi> getListVbcvFileVdi();

    List<VbcvVanBanDi> getListVbcvVanBanDi();

    List<EOffice> getListEOffice();

}
